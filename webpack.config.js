const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'development';
// paths
const srcPath = path.join(__dirname, 'src');
const buildPath = path.join(__dirname, 'build');

// common config
const common = {
  entry: {
    app: path.resolve(srcPath, 'index.js')
  },
  output: {
    path: buildPath,
    filename: '[name]-[hash].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: '/'
  },
  resolve: {
    modules: [srcPath, 'node_modules']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        exclude: /node_modules/,
        options: {
          configFile: '.eslintrc'
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?cacheDirectory',
        include: srcPath
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
        options: {
          limit: 1000
        }
      },
      {
        test: /\.(svg|woff|woff2|ttf|eot)$/,
        loader: 'file-loader',
        options: {
          name: 'assets/[path][name].[ext]',
          context: path.resolve(__dirname, 'src/assets')
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(nodeEnv)
    }),
    // concat all external modules in vendor file
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: ({ resource }) => /node_modules/.test(resource)
    }),
    new webpack.optimize.CommonsChunkPlugin('manifest'),
    new HtmlWebpackPlugin({
      template: path.join(srcPath, 'index.html')
    }),
    new webpack.NamedModulesPlugin()
  ]
};

let config;

// development
if (nodeEnv === 'development') {
  config = merge(common, {
    devtool: 'eval-source-map',
    module: {
      rules: [
        {
          test: /\.s?css$/,
          use: [
            { loader: 'style-loader', options: { sourceMap: true } },
            {
              loader: 'css-loader',
              options: { importLoaders: 2, sourceMap: true }
            },
            { loader: 'postcss-loader', options: { sourceMap: true } },
            { loader: 'sass-loader', options: { sourceMap: true } }
          ]
        }
      ]
    },
    plugins: [new webpack.HotModuleReplacementPlugin()],
    devServer: {
      contentBase: path.join(srcPath, 'index.html'),
      publicPath: '/',
      compress: true,
      port: 3000,
      hotOnly: true,
      noInfo: true,
      historyApiFallback: true
    }
  });

  // production
} else {
  config = merge(common, {
    module: {
      rules: [
        {
          test: /\.s?css$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: 'css-loader',
                options: { importLoaders: 2 }
              },
              'postcss-loader',
              'sass-loader'
            ]
          })
        }
      ]
    },
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true
      }),
      new ExtractTextPlugin('app-[hash].css'),
      new OptimizeCssAssetsPlugin()
    ]
  });
}

module.exports = config;
