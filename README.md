# Nimbl3

Nimbl3 frontend test

## Usage

* Run `npm install` to install all dependencies.
* Run `npm run dev` to compile and run development server.
* Run `npm run build` to create production files in **build** directory.
* Run `npm run build:serve` to serve latest production build.
