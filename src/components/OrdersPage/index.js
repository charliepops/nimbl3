import React from 'react';
import Additions from 'components/Additions';
import OrderSummary from 'components/OrderSummary';
import AddProduct from 'components/AddProduct';
import Remarks from 'components/Remarks';
import OrdersList from 'components/OrdersList';
import Footer from 'components/Footer';

const OrdersPage = () => (
  <div>
    <Additions />
    <OrderSummary />
    <AddProduct />
    <Remarks />
    <OrdersList />
    <Footer />
  </div>
);

export default OrdersPage;
