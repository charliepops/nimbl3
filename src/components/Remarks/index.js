import './index.scss';
import React from 'react';

const Remarks = () => (
  <div className="remarks">
    <h3>Remarks</h3>
    <p>
      When you enter into any new area of science, you almost always find
      yourself with a baffling new language of technical terms to learn before
      you can converse with the experts.
    </p>
  </div>
);

export default Remarks;
