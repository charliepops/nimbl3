import './index.scss';
import React from 'react';

const OrderSummary = () => (
  <div className="order-summary">
    <div className="order-summary-wrapper">
      <form>
        <div className="grid-container full">
          <div className="order-summary-top">
            <div className="grid-x">
              <div className="small-6 cell">
                <label>Estimated Delivery Date</label>
              </div>
              <div className="small-4 small-offset-2 cell">
                <input type="text" defaultValue="14-04-2016" />
              </div>
            </div>
          </div>

          <div className="order-summary-content">
            <div className="grid-x">
              <div className="small-6 cell">
                <label>Sub-total</label>
              </div>
              <div className="small-4 small-offset-2 cell">
                <span>40,033</span>
              </div>
            </div>

            <div className="grid-x">
              <div className="small-6 cell">
                <label>Shipping</label>
              </div>
              <div className="small-4 small-offset-2 cell">
                <input type="text" defaultValue="0" />
              </div>
            </div>

            <div className="grid-x">
              <div className="small-6 cell">
                <label>Special Discount</label>
              </div>
              <div className="small-4 small-offset-2 cell">
                <input type="text" defaultValue="9,999.999" />
              </div>
            </div>

            <div className="grid-x">
              <div className="small-6 cell">
                <label>Tax</label>
              </div>
              <div className="small-4 small-offset-2 cell">
                <div className="grid-container full">
                  <div className="grid-x tax">
                    <div className="small-3 cell">
                      <input type="text" defaultValue="7" />
                    </div>
                    <div className="small-3 cell">
                      <span className="float-left text-center">%</span>
                    </div>
                    <div className="small-6 cell">
                      <span>1,860.00</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="order-summary-bottom">
            <div className="grid-x">
              <div className="small-6 cell">
                <label>Total</label>
              </div>
              <div className="small-6 cell">
                <span>THB 41,585.31</span>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
);

export default OrderSummary;
