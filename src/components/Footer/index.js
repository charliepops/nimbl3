import './index.scss';
import imgSrc from 'assets/images/poweredby.svg';
import React from 'react';

const Footer = () => (
  <footer className="footer">
    <img src={imgSrc} alt="nimbl3" />
  </footer>
);

export default Footer;
