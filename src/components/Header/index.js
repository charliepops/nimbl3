import './index.scss';
import React from 'react';

export const Header = () => (
  <div className="header">
    <div className="grid-x">
      <div className="small-2 cell">
        <i className="header-back icon-arrow" />
      </div>
      <div className="auto cell">
        <h1 className="header-title">Edit ORD07070707</h1>
      </div>
      <div className="small-2 cell">
        <i className="header-search icon-magnifying-glass" />
      </div>
    </div>
  </div>
);

export default Header;
