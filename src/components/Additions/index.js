import './index.scss';
import React from 'react';

const Additions = () => (
  <div className="additions">
    <div className="additions-place">
      <h2 className="additions-header">CPF Saraburi</h2>
      <p className="additions-subheader">
        Highway 2, Kaeng Khoi Saraburi Thailand
      </p>
    </div>
    <div className="additions-update">
      <form className="additions-form">
        <button type="button" className="button">
          Update Order
        </button>
        <label>Created by</label>
        <select>
          <option>Tom Hanks</option>
          <option>Chespirito</option>
        </select>
        <label>Status</label>
        <select>
          <option>Pending</option>
          <option>In Progress</option>
          <option>Done</option>
        </select>
      </form>
      <hr />
    </div>
  </div>
);

export default Additions;
