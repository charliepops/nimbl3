import React from 'react';
import Header from 'components/Header';
import OrdersPage from 'components/OrdersPage';

const App = () => (
  <div>
    <Header />
    <main>
      <OrdersPage />
    </main>
  </div>
);

export default App;
