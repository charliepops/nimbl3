import './index.scss';
import React from 'react';

const OrdersList = () => (
  <div className="table-scroll orders-list">
    <table>
      <thead>
        <tr>
          <th>SKU</th>
          <th>Description</th>
          <th>Brand</th>
          <th>List Price</th>
          <th>Discount</th>
          <th>Net Price</th>
          <th>Qty</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {[1, 2, 3, 4, 5].map(i => {
          return (
            <tr key={i}>
              <td>TN SO 000015</td>
              <td>
                Magnetic contactor - 9A (5.5 kW\, 7.5 HP)\, control voltage 230
                Vac
              </td>
              <td>Schneider Electric</td>
              <td>THB 880</td>
              <td>30%</td>
              <td>THB 660</td>
              <td>16</td>
              <td>THB 50,079,8823</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>
);

export default OrdersList;
