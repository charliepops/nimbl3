import './index.scss';
import React from 'react';

const AddProduct = () => (
  <div className="add-product">
    <h2>
      <i className="icon-icon-plus" /> Add Products
    </h2>
  </div>
);

export default AddProduct;
