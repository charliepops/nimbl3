import 'index.scss';
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import App from 'components/App';

const render = App => {
  ReactDOM.render(<App />, document.getElementById('app-container'));
};

if (module.hot) {
  module.hot.accept('components/App', () => {
    render(App);
  });
}

render(App);
